package tycl.chapter1;

import org.junit.Test;
import tycl.HelloWorld;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class HowToUse {

    @Test
    public void staticInvoke() throws Exception {
        ClassLoader classLoader = HowToUse.class.getClassLoader();
        @SuppressWarnings("unchecked")
        Class<HelloWorld> clazz = (Class<HelloWorld>) classLoader.loadClass("tycl.HelloWorld");
        Method m = clazz.getDeclaredMethod("helloStatic");

        String ret = (String) m.invoke(null);
        assertThat(ret, is("hello, world"));
    }

    @Test
    public void instantiate() throws Exception {
        ClassLoader classLoader = HowToUse.class.getClassLoader();
        @SuppressWarnings("unchecked")
        Class<HelloWorld> clazz = (Class<HelloWorld>) classLoader.loadClass("tycl.HelloWorld");
        Constructor<HelloWorld> constructor = clazz.getConstructor(String.class);
        HelloWorld instance = constructor.newInstance("reflection");

        String ret = instance.hello();
        assertThat(ret, is("hello, reflection"));
    }

}
