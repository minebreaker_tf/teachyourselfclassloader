package tycl;

public class HelloWorld {

    private String name;

    public HelloWorld(String name) {
        this.name = name;
    }

    public static String helloStatic() {
        return "hello, world";
    }

    public String hello() {
        return "hello, " + name;
    }

}
