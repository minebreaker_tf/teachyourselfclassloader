package tycl.chapter3;

public final class MyClassLoader extends ClassLoader {

    private final byte[] clazz;

    public MyClassLoader( byte[] clazz ) {
        this.clazz = clazz;
    }

    @Override
    protected Class<?> findClass( String name ) throws ClassNotFoundException {
        return defineClass( "tycl.chapter3.Sample", clazz, 0, clazz.length );
    }

}
