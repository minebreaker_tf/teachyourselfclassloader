package tycl.chapter3;

import org.junit.Test;

import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public final class MultipleStatic {

    @Test
    public void test() throws Exception {

        Path path = Paths.get( "./build/classes/main/tycl/chapter3/Sample.class" );
        byte[] clazz = Files.readAllBytes( path );

        MyClassLoader v1 = new MyClassLoader( clazz );
        MyClassLoader v2 = new MyClassLoader( clazz );
        Field f1 = v1.loadClass( "" ).getDeclaredField( "version" );
        Field f2 = v2.loadClass( "" ).getDeclaredField( "version" );

        assertThat( f1.get( null ), is( "version1" ) );
        assertThat( f2.get( null ), is( "version1" ) );

        f2.set( null, "version2" );

        assertThat( f1.get( null ), is( "version1" ) );
        assertThat( f2.get( null ), is( "version2" ) );
    }

}
