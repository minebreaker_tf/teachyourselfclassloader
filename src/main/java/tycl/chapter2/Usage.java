package tycl.chapter2;

import org.junit.Test;
import tycl.HelloWorld;

import java.lang.reflect.Method;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public final class Usage {

    @Test
    public void className() throws Exception {
        ClassLoader classLoader = new SelfMadeClassLoader();

        // Automatically delegated to parent class loader
        @SuppressWarnings("unchecked")
        Class<HelloWorld> clazz = (Class<HelloWorld>) classLoader.loadClass("tycl.chapter1.HowToUse");

        assertThat(clazz.getCanonicalName(), is("tycl.chapter1.HowToUse"));

        Method[] m = clazz.getDeclaredMethods();
        assertThat(m[0].getName(), is("instantiate"));
        assertThat(m[1].getName(), is("staticInvoke"));
    }

    @Test
    public void staticInvoke() throws Exception {
        ClassLoader classLoader = new SelfMadeClassLoader();

        // If the parent class loader can not load the class, SelfMadeClassLoader.findClass is called.
        //
        // Check with debugger!!
        @SuppressWarnings("unchecked")
        Class<HelloWorld> clazz = (Class<HelloWorld>) classLoader.loadClass("classThatDoesNotExist");

        // SelfMadeClassLoader always returns HelloWorld class, whatever name is specified.
        assertThat(clazz.getCanonicalName(), is("tycl.HelloWorld"));

        Method m = clazz.getDeclaredMethod("helloStatic");
        String ret = (String) m.invoke(null);
        assertThat(ret, is("hello, world"));
    }

}
