package tycl.chapter2;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class SelfMadeClassLoader extends ClassLoader {

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        Path path = Paths.get("./build/classes/main/tycl/HelloWorld.class");
        try {
            byte[] clazz = Files.readAllBytes(path);
            return defineClass("tycl.HelloWorld", clazz, 0, clazz.length);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
